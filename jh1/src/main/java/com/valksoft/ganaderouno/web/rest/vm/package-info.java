/**
 * View Models used by Spring MVC REST controllers.
 */
package com.valksoft.ganaderouno.web.rest.vm;
