import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { GanaderoUnoSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [GanaderoUnoSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [GanaderoUnoSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GanaderoUnoSharedModule {
  static forRoot() {
    return {
      ngModule: GanaderoUnoSharedModule
    };
  }
}
