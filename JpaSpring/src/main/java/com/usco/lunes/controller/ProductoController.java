package com.usco.lunes.controller;

import com.usco.lunes.entity.Cliente;
import com.usco.lunes.entity.Producto;
import com.usco.lunes.service.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;

@Controller
@SessionAttributes("producto")
public class ProductoController {

    @Autowired
    private IClienteService clienteService;



    @RequestMapping(value = "/producto", method = RequestMethod.GET)
    public String listar(Model model) {
        model.addAttribute("titulo", "Listado de productos");
        model.addAttribute("tituloCrear", "Crear producto");
        model.addAttribute("productos", clienteService.findAllProductos());
        return ("producto/producto");
    }

    @RequestMapping(value = "/eliminarProducto/{id}")
    public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash) {

        try {
            if (id > 0) {
                clienteService.deleteProducto(id);
                flash.addFlashAttribute("success", "Producto eliminado con éxito");

            }
        } catch (Exception ex) {
            flash.addFlashAttribute("error",
                    "No se puede eliminar el registro " + id + " directamente");

            System.out.println("Something went wrong.");
        }


        return "redirect:/producto";
    }

    // --- nuevo código

    @RequestMapping(value = "/productoForm")
    public String crear(Map<String, Object> model) {
        Producto producto = new Producto();
        model.put("producto", producto);
        model.put("titulo", "Crear Producto");
        return ("producto/formProducto");
    }

    @RequestMapping(value = "/productoForm", method = RequestMethod.POST)
    public String guardar(@Valid Producto producto, BindingResult result, Model model, SessionStatus status) {

        clienteService.saveProducto(producto);

        // al invocar setComplete va a eliminar al objeto encargado "el SessionAttibute
        // articulo"
        status.setComplete();
        return "redirect:/producto";
    }



    @RequestMapping(value = "/productoForm/{id}")
    public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {

        Producto producto = null;

        if (id > 0) {
            producto = clienteService.findOneProducto(id);
        } else {
            return "redirect:/producto";
        }
        model.put("producto", producto);
        model.put("titulo", "Editar Artículo");
        return ("producto/formProducto");
    }


}
