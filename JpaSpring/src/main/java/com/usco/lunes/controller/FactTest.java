package com.usco.lunes.controller;

import com.usco.lunes.entity.Factura;
import com.usco.lunes.service.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Map;

@Controller
@SessionAttributes("factdeta")
public class FactTest {

    @Autowired
    private IClienteService clienteService;

    @RequestMapping(value = "/listarFactura")
    public String listar(Model model){
        model.addAttribute("titulo", "Listado de facturas");
        model.addAttribute("facturas", clienteService.findAllFactura());
        return ("factura/factura");
    }

    @RequestMapping(value = "/detalleFactura/{id}")
    public String ver(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash) {
        System.out.println("test");
        Factura factura = clienteService.findOneFactura(id);

        model.put("factura", factura);
        model.put("titulo", "Detalle factura: " + factura.getDescripcion());

        return ("factura/detalleFactura");
    }




}